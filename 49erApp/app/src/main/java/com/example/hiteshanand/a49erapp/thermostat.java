package com.example.hiteshanand.a49erapp;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class thermostat extends AppCompatActivity {
int fan_val = 0, mode_val = 0;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_thermostat);
    }
    void fanon(View view){ fan_val = 1;  }
    void fanoff(View view){ fan_val = 0; }
    void heatmode(View view){ mode_val = 1; }
    void coolmode(View view){ mode_val = 0; }

    void apply(View view) {
        EditText temperature = findViewById(R.id.Temperature_Value);
        final String Temperature = temperature.getText().toString();

        StringRequest stringRequest = new StringRequest(Request.Method.POST, Constants.URL_THERMOSTAT, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                try{JSONObject jsonObject = new JSONObject(response);
                    Toast.makeText(getApplicationContext(),jsonObject.getString("message"),Toast.LENGTH_LONG).show();

                }catch(JSONException e){
                    e.printStackTrace();

                }


            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // progressDialog.hide();
                        Toast.makeText(getApplicationContext(),error.getMessage(),Toast.LENGTH_LONG).show();
                    }
                }
        ){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String> params = new HashMap<>();
                params.put("temperature", Temperature);
                params.put("fan", Integer.toString(fan_val));
                params.put("mode", Integer.toString(mode_val));
                return params;
            }
        };

        RequestHandler.getInstance(this).addToRequestQueue(stringRequest);
    }
}
