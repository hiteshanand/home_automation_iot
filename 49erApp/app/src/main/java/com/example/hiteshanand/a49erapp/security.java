package com.example.hiteshanand.a49erapp;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

public class security extends AppCompatActivity {
    String res;
    String print = "No movement on first floor";
    String print1 = "No movement on Second Floor";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_security);

        StringRequest stringRequest = new StringRequest(Request.Method.POST, Constants.URL_FlOOR1SENSOR, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                try{JSONObject jsonObject = new JSONObject(response);
                    //Toast.makeText(getApplicationContext(),jsonObject.getString("message"),Toast.LENGTH_LONG).show();
                   res = jsonObject.getString("message");
                    if(res.charAt(11)=='1' || res.charAt(23)=='1'){
                        print = " Movement Detected on the first floor";
                    }
                    TextView textView = (TextView) findViewById(R.id.fl1sensor);
                    textView.setText(print);

                }catch(JSONException e){
                    e.printStackTrace();

                }


            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // progressDialog.hide();
                        Toast.makeText(getApplicationContext(),error.getMessage(),Toast.LENGTH_LONG).show();
                    }
                }
        ){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String> params = new HashMap<>();
                return params;
            }
        };

        RequestHandler.getInstance(this).addToRequestQueue(stringRequest);


        StringRequest stringRequest1 = new StringRequest(Request.Method.POST, Constants.URL_FlOOR2SENSOR, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                try{JSONObject jsonObject = new JSONObject(response);
                    //Toast.makeText(getApplicationContext(),jsonObject.getString("message"),Toast.LENGTH_LONG).show();
                    res = jsonObject.getString("message");
                    if(res.charAt(11)=='1' || res.charAt(23)=='1'){
                        print1 = " Movement Detected on the Second floor";
                    }
                    TextView textView = (TextView) findViewById(R.id.fl2sensor);
                    textView.setText(print1);

                }catch(JSONException e){
                    e.printStackTrace();

                }


            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // progressDialog.hide();
                        Toast.makeText(getApplicationContext(),error.getMessage(),Toast.LENGTH_LONG).show();
                    }
                }
        ){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String> params = new HashMap<>();
                // params.put("username", username);
                //params.put("password",password);
                return params;
            }
        };

        RequestHandler.getInstance(this).addToRequestQueue(stringRequest1);


    }
    void update_security(View view){
        this.recreate();
    }
    
}
