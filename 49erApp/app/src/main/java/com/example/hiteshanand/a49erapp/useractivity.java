package com.example.hiteshanand.a49erapp;

import android.content.Intent;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ThreadLocalRandom;

@RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)


public class useractivity extends AppCompatActivity {

    Switch simpleSwitch1, simpleSwitch2;
    int min = 40, max = 60;
    int Light11 = 0 , Light12 = 0, Light21 = 0, Light22 = 0;
    int power = ThreadLocalRandom.current().nextInt(min, max + 1);
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_useractivity);
    }
    void first_floor(View view){
        setContentView(R.layout.first_floor);
    }
    void second_floor(View view){
        setContentView(R.layout.second_floor);
    }

    void backtouser(View view){
        setContentView(R.layout.activity_useractivity);
    }

    public void security_function(View view){
        Intent intent = new Intent(this, security.class);
        startActivity(intent);
    }


    void power(View view){
        Intent intent = new Intent(this,PowerGenerator.class);
        startActivity(intent);
    }
     void Thermostat(View view){
        Intent intent = new Intent(this,thermostat.class);
        startActivity(intent);
     }
    void weather(View view){
        setContentView(R.layout.weather);
        Intent intent = new Intent(this,weather.class);
        startActivity(intent);
    }
    void securitycamera(View view){
        Intent intent = new Intent(this,securitycamera.class);
        startActivity(intent);
    }
    void updateserverf1(View view){

        StringRequest stringRequest = new StringRequest(Request.Method.POST, Constants.URL_FLOOR1, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                try{JSONObject jsonObject = new JSONObject(response);
                    Toast.makeText(getApplicationContext(),jsonObject.getString("message"),Toast.LENGTH_LONG).show();

                }catch(JSONException e){
                    e.printStackTrace();

                }


            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // progressDialog.hide();
                        Toast.makeText(getApplicationContext(),error.getMessage(),Toast.LENGTH_LONG).show();
                    }
                }
        ){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String> params = new HashMap<>();
                params.put("light1", Integer.toString(Light11));
                params.put("light2", Integer.toString(Light12));
                return params;
            }
        };

        RequestHandler.getInstance(this).addToRequestQueue(stringRequest);
    }

    void updateserverf2(View view){

        StringRequest stringRequest = new StringRequest(Request.Method.POST, Constants.URL_FlOOR2, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                try{JSONObject jsonObject = new JSONObject(response);
                    Toast.makeText(getApplicationContext(),jsonObject.getString("message"),Toast.LENGTH_LONG).show();

                }catch(JSONException e){
                    e.printStackTrace();

                }


            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // progressDialog.hide();
                        Toast.makeText(getApplicationContext(),error.getMessage(),Toast.LENGTH_LONG).show();
                    }
                }
        ){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String> params = new HashMap<>();
                params.put("light1", Integer.toString(Light21));
                params.put("light2", Integer.toString(Light22));
                return params;
            }
        };

        RequestHandler.getInstance(this).addToRequestQueue(stringRequest);
    }

    void F1L1on(View view){
        Light11 = 1;
    }
    void F1L1off(View view){
        Light11 = 0;
    }
    void F1L2on(View view){
        Light12 = 1;
    }
    void F1L2off(View view){
        Light12 = 0;
    }

    void F2L1on(View view){
        Light21 = 1;
    }
    void F2L1off(View view){
        Light21 = 0;
    }
    void F2L2on(View view){
        Light22 = 1;
    }
    void F2L2off(View view){
        Light22 = 0;
    }

}
