package com.example.hiteshanand.a49erapp;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class PowerGenerator extends AppCompatActivity {

    String res,templight1,templight2, light1,light2,tempfan,fan;
    public int powerValue = 0;
    String[] tempfloor1light,tempfloor2light;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.powergeneratedlayout);

        StringRequest stringRequest = new StringRequest(Request.Method.POST, Constants.URL_FLOOR1_UTILITY_LED, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                try {
                    JSONObject jsonObject = new JSONObject(response);
                    //Toast.makeText(getApplicationContext(),jsonObject.getString("message"),Toast.LENGTH_LONG).show();
                    res = jsonObject.getString("message");

                    tempfloor1light = res.split(",");
                    templight1 = tempfloor1light[0].replace("{\"", "");
                    light1 = templight1.replace("\"", "");
                    templight2 = tempfloor1light[1].replace("\"", "");
                    light2 = templight2.replace("}", "");

                    if (light1.equalsIgnoreCase("light1:1"))
                        powerValue = powerValue + 10;
                    if (light2.equalsIgnoreCase("light2:1"))
                        powerValue = powerValue +  10;

                } catch (JSONException e) {
                    e.printStackTrace();

                }
            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // progressDialog.hide();
                        Toast.makeText(getApplicationContext(), error.getMessage(), Toast.LENGTH_LONG).show();
                    }
                }
        ) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                return params;
            }
        };
        RequestHandler.getInstance(this).addToRequestQueue(stringRequest);

        StringRequest stringRequest1 = new StringRequest(Request.Method.POST, Constants.URL_FLOOR2_UTILITY_LED, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                try {
                    JSONObject jsonObject = new JSONObject(response);
                    //Toast.makeText(getApplicationContext(),jsonObject.getString("message"),Toast.LENGTH_LONG).show();
                    res = jsonObject.getString("message");

                    tempfloor2light = res.split(",");
                    templight1 = tempfloor2light[0].replace("{\"", "");
                    light1 = templight1.replace("\"", "");
                    templight2 = tempfloor2light[1].replace("\"", "");
                    light2 = templight2.replace("}", "");

                    if (light1.equalsIgnoreCase("light1:1"))
                        powerValue = powerValue +  10;
                    if (light2.equalsIgnoreCase("light2:1"))
                        powerValue = powerValue +  10;

                } catch (JSONException e) {
                    e.printStackTrace();

                }
            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // progressDialog.hide();
                        Toast.makeText(getApplicationContext(), error.getMessage(), Toast.LENGTH_LONG).show();
                    }
                }
        ) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                return params;
            }
        };
        RequestHandler.getInstance(this).addToRequestQueue(stringRequest1);

        StringRequest thermoStringRequest = new StringRequest(Request.Method.POST, Constants.URL_THERMOSTAT_UTILITY, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                try {
                    JSONObject jsonObject = new JSONObject(response);
                    //Toast.makeText(getApplicationContext(),jsonObject.getString("message"),Toast.LENGTH_LONG).show();
                    res = jsonObject.getString("message");

                    tempfan = res.replace("{\"", "");
                    fan = tempfan.replace("\"", "");
                    fan = fan.replace("}", "");

                    if (fan.equalsIgnoreCase("fan:1"))
                        powerValue = powerValue +  20;

                    TextView textView = (TextView) findViewById(R.id.textView_Powervalue);
                    textView.setText(Integer.toString(powerValue));

                } catch (JSONException e) {
                    e.printStackTrace();

                }
            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // progressDialog.hide();
                        Toast.makeText(getApplicationContext(), error.getMessage(), Toast.LENGTH_LONG).show();
                    }
                }
        ) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                return params;
            }
        };
        RequestHandler.getInstance(this).addToRequestQueue(thermoStringRequest);


    }
}
