package com.example.hiteshanand.a49erapp;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class Utility_Activity extends AppCompatActivity {
    String res,templight1,templight2, light1,light2,tempfan,fan;
    String[] tempfloor1light,tempfloor2light;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_utility);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Constants.URL_FLOOR1_UTILITY_LED, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                try {
                    JSONObject jsonObject = new JSONObject(response);
                    //Toast.makeText(getApplicationContext(),jsonObject.getString("message"),Toast.LENGTH_LONG).show();
                    res = jsonObject.getString("message");

                    tempfloor1light = res.split(",");
                    templight1 = tempfloor1light[0].replace("{\"","");
                    light1 = templight1.replace("\"","");
                    templight2 = tempfloor1light[1].replace("\"","");
                    light2 = templight2.replace("}","");

                    TextView textView = (TextView) findViewById(R.id.light1Utility);
                    textView.setText("Floor 1 - " + light1);
                    TextView textView1 = (TextView) findViewById(R.id.light2Utility);
                    textView1.setText("Floor 1 - " + light2);

                } catch (JSONException e) {
                    e.printStackTrace();

                }
            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // progressDialog.hide();
                        Toast.makeText(getApplicationContext(), error.getMessage(), Toast.LENGTH_LONG).show();
                    }
                }
        ) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                return params;
            }
        };
        RequestHandler.getInstance(this).addToRequestQueue(stringRequest);

        StringRequest stringRequest1 = new StringRequest(Request.Method.POST, Constants.URL_FLOOR2_UTILITY_LED, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                try {
                    JSONObject jsonObject = new JSONObject(response);
                    //Toast.makeText(getApplicationContext(),jsonObject.getString("message"),Toast.LENGTH_LONG).show();
                    res = jsonObject.getString("message");

                    tempfloor2light = res.split(",");
                    templight1 = tempfloor2light[0].replace("{\"","");
                    light1 = templight1.replace("\"","");
                    templight2 = tempfloor2light[1].replace("\"","");
                    light2 = templight2.replace("}","");

                    TextView textView = (TextView) findViewById(R.id.light3Utility);
                    textView.setText("Floor 2 - " + light1);
                    TextView textView1 = (TextView) findViewById(R.id.light4Utility);
                    textView1.setText("Floor 2 - " + light2);

                } catch (JSONException e) {
                    e.printStackTrace();

                }
            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // progressDialog.hide();
                        Toast.makeText(getApplicationContext(), error.getMessage(), Toast.LENGTH_LONG).show();
                    }
                }
        ) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                return params;
            }
        };
        RequestHandler.getInstance(this).addToRequestQueue(stringRequest1);

        StringRequest thermoStringRequest = new StringRequest(Request.Method.POST, Constants.URL_THERMOSTAT_UTILITY, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                try {
                    JSONObject jsonObject = new JSONObject(response);
                    //Toast.makeText(getApplicationContext(),jsonObject.getString("message"),Toast.LENGTH_LONG).show();
                    res = jsonObject.getString("message");

                    tempfan = res.replace("{\"","");
                    fan = tempfan.replace("\"","");
                    fan = fan.replace("}","");

                    TextView textView = (TextView) findViewById(R.id.thermUtility);
                    textView.setText("Thermostat " + fan);
                } catch (JSONException e) {
                    e.printStackTrace();

                }
            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // progressDialog.hide();
                        Toast.makeText(getApplicationContext(), error.getMessage(), Toast.LENGTH_LONG).show();
                    }
                }
        ) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                return params;
            }
        };
        RequestHandler.getInstance(this).addToRequestQueue(thermoStringRequest);
    }
    void refresh(View view){
        this.recreate();
    }
}
