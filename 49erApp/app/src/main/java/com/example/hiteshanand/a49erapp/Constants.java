package com.example.hiteshanand.a49erapp;

public class Constants {
    public static final String ROOT_URL = "http://10.220.122.133/Android/v1/";
    public static final String URL_REGISTER = ROOT_URL + "/registerUser.php";
    public static final String URL_LOGIN = ROOT_URL + "userLogin.php";
    public static final String URL_FLOOR1 = ROOT_URL + "floor1.php";
    public static final String URL_FlOOR2 = ROOT_URL + "floor2.php";
    public static final String URL_FlOOR1SENSOR = ROOT_URL + "floor1getsensor.php";
    public static final String URL_FlOOR2SENSOR = ROOT_URL + "floor2getsensor.php";
    public static final String URL_FLOOR1_UTILITY_LED = ROOT_URL + "floor1get.php";
    public static final String URL_FLOOR2_UTILITY_LED = ROOT_URL + "floor2get.php";
    public static final String URL_THERMOSTAT_UTILITY = ROOT_URL + "thermostatget.php";
    public static final String URL_THERMOSTAT = ROOT_URL + "thermostat.php";
    public static final String CAMERA_URL = "http://10.220.122.133/UploadExample/";
    public static final String URL_SECURITYCAM = CAMERA_URL + "getImage.php";


}