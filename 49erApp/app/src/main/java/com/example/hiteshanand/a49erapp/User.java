package com.example.hiteshanand.a49erapp;


/**
 * Created by Belal on 9/5/2017.
 */


//this is very simple class and it only contains the user attributes, a constructor and the getters
// you can easily do this by right click -> generate -> constructor and getters
public class User {

    private String username;

    public User(String username) {

        this.username = username;
    }

    public String getUsername() {
        return username;
    }

}
